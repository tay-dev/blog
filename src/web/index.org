#+TITLE: Web Development Articles
#+HTML_LINK_UP: /
#+DESCRIPTION: Web development articles
#+CREATED: <2022-10-02 Sun>

1. [[file:sass-mixins.org][Sass Mixins]]

   Switch layout and router mixins for sass+react.
 
2. [[file:nginx-tracking.org][Nginx Tracking with MySQL]]

   Log Nginx requests directly to MySQL for easy querying.

4. [[file:nav-bar.org][Creating a nav bar]]

   Create a nav-bar from scratch using vanilla javascript, css and
   html.

5. [[file:shiftkeys.org][The Shiftkeys Game]]

   Learn to use the opposing shift key from the letter you are
   pressing.

6. [[file:webpack-for-a-working-website.org][Webpack for an Already-Working Website]]

   Add webpack to a vanilla javascript website without breaking it.

