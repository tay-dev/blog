#+TITLE: Emacs Articles
#+DESCRIPTION: Amy Grinn's Emacs articles.
#+CREATED: <2022-09-24 Sat>
#+HTML_LINK_UP: /

1. [[file:blog.org][How This Blog Works]]

   How to set up an Org blog using nginx.

2. [[file:org-real.org][Org Real]]

   Keep track of real things as Org mode links.

3. [[file:org-yaap.org][Org Yaap]]

   Yet another alert package for Org.

4. [[file:packages.org][Emacs Packages]]

   A list of packages I've started.

5. [[file:org-todo-link.org][Org TODO Link]]

   Show the TODO status of a linked heading in Org mode.

6. [[file:mode-line.org][Custom Mode Line Format]]

   Fancy non-blocking mode line items.

7. [[file:cursor.org][I Have Strong Opinions on the Shape of Your Cursor]]

8. [[file:workflow.org][My Workflow]]

   Personal project planning.

9. [[file:bash-functions.org][Update Emacs with Bash Functions]]

   Build Emacs from scratch.

10. [[file:eventuel.org][My First Package: Eventuel]]

    A journal of the stumbles and triumphs of my first elisp package.

