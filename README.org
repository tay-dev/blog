#+TITLE: Amy's Blog

* Git

  #+begin_src sh
    # Clone from Gitlab
    git clone -o gitlab git@gitlab.com:tay-dev/blog.git

    # Add production server as a remote
    cd blog
    git remote add prod grinn@<ip>:/git/blog.git
    git config --local remote.prod.receivepack "powershell git-recieve-pack"
    git config --local remote.prod.uploadpack "powershell git-upload-pack"
  #+end_src

  Always update the Gitlab remote before the production server.

* Commands

  Use the command =./project= to see available commands for building
  the website.
