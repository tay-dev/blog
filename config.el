;; config.el --- Project configuration for my blog. -*- lexical-binding: t; -*-

;; Author: Amy Grinn <grinn.amy@gmail.com>
;; File: config.el
;; Version: 1
;; Package-Requires: ((emacs "28"))

;;; Commentary:

;; Project configuration for my blog.

;;; Code:

;;;; Project configuration

(setq base-url "https://blog.amygrinn.com/")

;;;; Requirements:

(require 'package)

(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)

(setq package-selected-packages '(htmlize nginx-mode yaml-mode))

(package-initialize)

(dolist (package package-selected-packages)
  (unless (package-installed-p package)
    (condition-case err
        (package-install package)
      (error
       (if (string-match-p "Package ‘[^’]+’ is unavailable" (cadr err))
           (progn
             (package-refresh-contents)
             (package-install package))
         (error (error-message-string err)))))))

(require 'ox-html)

;;;; Configure org export

(setq org-html-extension "")
(setf (alist-get "html" org-html-xml-declaration nil nil #'equal) "")
(setq org-html-doctype "html5")
(setq org-html-home/up-format
      "<div id=\"org-div-home-and-up\" style=\"display: flex;\">
  <span style=\"margin-right: 1em;\">Amy Grinn</span>
  <a accesskey=\"u\" href=\"%s\"> UP </a>
  <span>|</span>
  <a accesskey=\"h\" href=\"%s\"> HOME </a>
  <a accesskey=\"m\" rel=\"me\" href=\"https://emacs.ch/@grinn\" style=\"margin-left: auto;\">emacs.ch</a>
</div>")

;; Display options:
(setq org-export-with-section-numbers nil
      org-export-with-toc nil
      org-html-validation-link nil
      org-image-actual-width nil)

;; CSS
(setq org-html-head-include-default-style nil)
(setq org-html-htmlize-output-type 'css)

;; Navigation:
(setq org-html-link-home "/"
      org-html-link-up ".")

;; Time format
(setq org-html-metadata-timestamp-format "%Y-%m-%d")

;; HTML head:
(setq org-html-head "<link rel=\"stylesheet\" type=\"text/css\" href=\"/main.css\"/>
<link rel=\"apple-touch-icon\" sizes=\"180x180\" href=\"/favicon/apple-touch-icon.png\"/>
<link rel=\"icon\" type=\"image/x-icon\" href=\"/favicon/favicon.ico\"/>
<link rel=\"icon\" type=\"image/png\" sizes=\"32x32\" href=\"/favicon/favicon-32x32.png\"/>
<link rel=\"icon\" type=\"image/png\" sizes=\"16x16\" href=\"/favicon/favicon-16x16.png\"/>
<link rel=\"manifest\" href=\"/manifest.json\"/>
<link rel=\"preconnect\" href=\"https://fonts.googleapis.com\">
<link rel=\"preconnect\" href=\"https://fonts.gstatic.com\" crossorigin>
<link rel=\"preload\" as=\"style\" href=\"https://fonts.googleapis.com/css2?family=Balsamiq+Sans&family=Handlee&family=Kantumruy+Pro:wght@300&family=Noto+Sans+Mono:wght@300&display=swap\">
<link id=\"google-fonts\" rel=\"stylesheet\" media=\"print\" href=\"https://fonts.googleapis.com/css2?family=Balsamiq+Sans&family=Handlee&family=Kantumruy+Pro:wght@300&family=Noto+Sans+Mono:wght@300&display=swap\">
<script>
 const link = document.getElementById('google-fonts')
 if (link.sheet)
   link.media = 'all';
 else
   link.addEventListener('load', function() { link.media = 'all'; });
</script>")

;; HTML postamble:
(setq org-html-postamble
      (lambda (info)
        "Returns the creation and last modified date in HTML from INFO plist."
        (when-let* ((file (plist-get info :input-file))
                    (timestamp-format (plist-get info :html-metadata-timestamp-format))
                    (last-modified (format-time-string timestamp-format
                                                        (file-attribute-modification-time
                                                         (file-attributes file))))
                    (postamble (format "<p>Last modified: <span class=\"data\">%s</span></p>
<p><a href=\"/privacy\">Privacy Policy</a></p>"
                                       last-modified)))
          (if-let* ((created-keyword (with-temp-buffer
                                       (insert-file-contents file)
                                       (cadr (car (org-collect-keywords '("CREATED"))))))
                    (created (format-time-string timestamp-format
                                                 (org-timestamp-to-time
                                                  (org-timestamp-from-string created-keyword)))))
              (concat (format "<p>Created: <span class=\"date\">%s</span></p>" created) postamble)
            postamble))))

;;; config.el ends here
